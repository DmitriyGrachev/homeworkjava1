package com.company.one;

public class Part2 {
    public static void main(String[] args){
        //1. Посчитать среднее арифметическое массива чисел.
        int[] array1 = {1,14,32,43,43,54,12,12,3};
        double res = 0;
        for (int i = 0; i < array1.length; i++){
            res = res + array1[i];
        }
        for (int i = 0; i < array1.length; i++){
            System.out.print(array1[i] + " ");
        }
        System.out.println();
        res = res / array1.length;
        System.out.println("Среднее арифметическое = "+res);
        //2. Найти максимальный и минимальный элементы массива.
        int max = array1[0];
        int min = array1[0];
        for (int i = 0; i < array1.length; i++){
            if(array1[i] > max){
                max = array1[i];
            }
        }
        for (int i = 0; i < array1.length; i++){
            if(array1[i] < min){
                min = array1[i];
            }
        }
        System.out.println("Максимальное число =" + max);
        System.out.println("Минимальное число =" + min);
        //3. Найти повторяющиеся элементы в массиве.
        for (int i = 0; i < array1.length; i++){
            for (int j = i + 1; j < array1.length;j++){
                if (array1[i] == array1[j]){
                    System.out.print(array1[i]+" ");
                }
            }
        }
        System.out.println();
        //4. Найти второе наибольшее значение в массиве.
        int maxV = array1[1];
        int smaxV = array1[0];
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] > maxV) {
                smaxV = maxV;
                maxV = array1[i];
            }else if (array1[i] > smaxV) {
                smaxV = array1[i];
            }
        }
        System.out.println("Второе найбольше число = " + smaxV);
        //5. Найти второе наименьшее значение в массиве.
        int minV = array1[0];
        int sminV = array1[1];
        for (int i = 0; i < array1.length; i++){
            if(array1[i] < minV){
                sminV  = minV;
                minV = array1[i];
            }else if (array1[i] > sminV) {
                sminV = array1[i];
            }
        }
        System.out.println("Второе найменьшее симло" + sminV);
        //6. Найти повторяющиеся значения в двух массивах.
        System.out.print("Повторяющиеся числа в двух массивах = ");
        int[] array2 = {12,34,56,78};
        for ( int i = 0; i < array1.length; i++){
            for (int j = 0; j < array2.length; j++){
                if (array1[i] == array2[j]){
                    System.out.print(array1[i]+ " ");
                }
            }
        }
        //7. Найти не повторяющиеся значения в двух массивах.
        System.out.println();
        System.out.print("Неповторяющиеся числа в двух массивах = ");
        boolean contains = false;
        for(int i=0; i<array1.length; i++) {
            for(int j=0; j<array2.length; j++) {
                if(array1[i]==array2[j]) {
                    contains = true;
                    break;
                }
            }
            if(!contains) {
                System.out.print(array1[i] + " ");
            }
            else{
                contains = false;
            }
        }
    }
}
