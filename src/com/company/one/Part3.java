package com.company.one;
import java.util.Scanner;
import java.util.Random;
public class Part3 {
    public static void main (String[] args) throws Exception{
        //1. Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
        System.out.println("Task1");
        int[] array1 = new int[20];
        array1[0] = 1;
        array1[1] = 1;
        System.out.print(array1[0] + " " + array1[1] + " ");
        for(int i = 2; i < array1.length; i++){
            array1[i] = array1[i-2] + array1[i-1];
            System.out.print(array1[i] + " ");
        }
        System.out.println();
        //2. Написать код, который заполнит массив произвольного размера числами по возрастанию,
        // начиная с центра массива, например, `[5,4,3,2,1,0,1,2,3,4,5]`.
        System.out.println("Task2");
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите размер масссива:");
        int size = scan.nextInt();
        int[] array2 = new int[size];
        int medium = size / 2;
        if (size % 2 != 0){
            array2[medium] = 0;
            for (int i = 0; i < array2.length; i++) {
                if (i < medium) {
                    array2[i] = medium - i;
                }else if (i > medium){
                    array2[i] = i - medium;
                }
            }
        }else if (size % 2 == 0){
            array2[medium-1] = 0;
            array2[medium] = 0;
            for (int i = 0; i < array2.length; i++){
                if (i < medium-1){
                    array2[i] = medium - i-1;
                }else if (i > medium){
                    array2[i] = i - medium;
                }
            }
        }
        for (int i = 0; i < array2.length; i++){
            System.out.print(array2[i] + " ");
        }
        //3. Найти в массиве число, которое повторяется наибольшее количество раз.
        System.out.println();
        System.out.println("Task3");
        int[] array3 = {1,1,4,5,6,7,7,7,7,8,9};
        int count = 1;
        int repeat = array3[0];
        int temp = 0;
        int tempCount;
        for (int i = 0; i < array3.length - 1; i++) {
            temp = array3[i];
            tempCount = 0;
            for (int j = 1; j < array3.length; j++) {
                if (temp == array3[j]) tempCount++;
            }
            if (tempCount > count) {
                repeat = temp;
                count = tempCount;
            }
        }
        System.out.println("Больше всех повторяется = " + repeat);
        //4. Написать код для зеркального переворота элементов
        // в массиве `([1, 2, 3, 4] -> [4, 3, 2, 1])`.
        System.out.println("Task4");
        int[] array4 = {1,3,4,6,12,65,2,3};
        for(int i = array4.length - 1; i >= 0; i--) {
            System.out.print(array4[i] + "  ");
        }
        //5. Создать двумерный массив из 8 строк по 5 столбцов в каждой из случайных целых чисел
        // из отрезка `[10;99]`. Вывести массив на экран.
        System.out.println();
        System.out.println("Task5");
        int[][] array5 = new int[8][5];
        Random rand = new Random();
        for (int i = 0; i < array5.length; i++){
            for (int j = 0; j < array5[i].length; j++){
                array5[i][j] = rand.nextInt(89)+ 10;
                System.out.print(array5[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
}

