package com.company.one;
import java.util.Random;
public class Part1 {
    public static void main(String[] args){
        //1.   Создайте массив интов без указания значений элементов при создании объекта.
        int[] array1 = new int[1];
        //2.   Создайте массив интов с указанием значений элементов при создании объекта.
        int[] array2 = {1,2,3};
        //3.   Создайте пустой массив интов. Заполните его любыми значениями.
        int[] array3 = new int [3];
        for(int i = 0;i < array3.length; i++){
            array3[i] = i;
        }
        System.out.println("Task4");
        //4.   Создайте пустой массив даблов. Заполните его любыми значениями и выведите в консоль содержимое.
        double[] array4 = new double[3];
        for(int i = 0;i < array4.length; i++){
            array4[i]=i;
        }
        for(int i = 0;i < array4.length; i++){
            System.out.print(array4[i]+" ");
        }
        System.out.println();
        System.out.println("Task5");
        //5.   Создайте пустой массив на 10 элементов. Заполните значения элементов. Расширьте массив на 5 элементов.
        int[] array5 = new int[10];
        for(int i = 0;i < array5.length; i++){
            array5[i]=i;
        }
        int[] array5ex = new int[array5.length+5];
        for(int i = 0;i < array5.length; i++){
            array5ex[i] = array5[i];
        }
        for(int i = 0;i < array5ex.length; i++){
            System.out.print(array5ex[i]+" ");
        }
        System.out.println();
        System.out.println("Task6");
        //6.   Создайте массив из 12 случайных целых чисел из отрезка `[-15;15]`.
        // Определите какой элемент является в этом массиве максимальным и сообщите индекс его последнего вхождения в массив
        int[] array6 = new int[12];
        for (int i = 0; i < array6.length; i++) {
            array6[i] = (int) Math.round((Math.random() * 30) - 15);
            System.out.print(array6[i]+" ");
        }
        //Найти найбольшее
        int max = array6[0];
        int temp = 0;
        for(int i = 1;i < array6.length; i++){
            if (max < array6[i]) {
                max = array6[i];
                temp = i;
            }
        }
        System.out.println("Максимальное значение = " + max + ". Индекс  = "+temp);
        System.out.println("Task7");
        //7.Создайте массив из 8 случайных целых чисел из отрезка `[1;10]`.
        // Выведите массив на экран в строку.
        // Замените каждый элемент с нечётным индексом на ноль.
        // Снова выведете массив на экран на отдельной строке
        Random rand = new Random();
        int[] array7 = new int[8];
        for(int i =0; i < array7.length; i++){
            array7[i] = rand.nextInt((10 - 1) + 1) + 1;
            System.out.print(array7[i]+" ");
        }
        for(int i = 0; i < array7.length; i++) {
            if (i % 2 != 0) {
                array7[i] = 0;
            }
        }
        System.out.println();
        for(int i = 0; i < array7.length; i++){
            System.out.print(array7[i]+" ");
        }
        System.out.println();
        System.out.println("Task8");
        //8.Создайте массив из 4 случайных целых чисел из отрезка `[10;99]`.
        // Выведите его на экран в строку.
        // Далее определите и выведите на экран сообщение о том, является ли массив строго возрастающей последовательностью.
        int[] array8 = new int[4];
        for(int i = 0; i < array8.length; i++){
            array8[i] = rand.nextInt((99-10)+10)+10;
            System.out.print(array8[i]+" ");
        }
        System.out.println();
        boolean type = true;
        for(int i = 1; i < array8.length; i++){
            if (array8[i] < array8[i-1]) {
                type = false;
                break;
            }
        }
        if(type){
            System.out.println("Массив строго возрастающий");
        }else{
            System.out.println("Массив не строго возрастающий");
        }
        System.out.println("Task9");
        //9.Создайте 2 массива из 5 случайных целых чисел из отрезка `[0;5]` каждый.
        // Выведите массивы на экран в двух отдельных строках.
        // Посчитайте среднее арифметическое элементов каждого массива и сообщите, для какого из массивов это значение оказалось больше (либо сообщите, что их средние арифметические равны).
        int[] array9 = new int[5];
        int[] array9second = new int[5];
        for (int i = 0; i < array9.length; i++) {
            array9[i] = rand.nextInt(5);
        }
        for (int i = 0; i < array9second.length; i++) {
            array9second[i] = rand.nextInt(5);
        }
        for(int i : array9){
            System.out.print(array9[i]+" ");
        }
        System.out.println();
        for(int i : array9second){
            System.out.print(array9second[i]+" ");
        }
        System.out.println();
        //Подсчет среднего арифметического
        double res1 = 0;
        double res2 = 0;
        for(int i = 0; i < array9.length; i++){
            res1 = res1 + array9[i];
        }
        for(int i = 0; i < array9second.length; i++){
            res2 = res2 + array9second[i];
        }
        res1 = res1 / 5;
        res2 = res2 / 5;
        if(res1 == res2){
            System.out.println("Средние арифметичские равны");
        }if(res1 > res2){
            System.out.println("Среднее арифметическое первого масссива больше");
        }if(res1 < res2){
            System.out.println("Среднее арифметическое второго массива больше");
        }
        System.out.println("Task10");
        //10.   Создать массив из 50 случайных целых чисел из отрезка `[0;1000]` и вывести его на экран.
        // Создать второй массив только из чётных элементов первого массива, если они там есть, и вывести его на экран.
        int[] array10 = new int[50];
        for (int i = 0; i < array10.length; i++) {
            array10[i] = rand.nextInt(1000);
        }
        for ( int i = 0; i < array10.length; i++){
            System.out.print(array10[i] + " ");
        }
        System.out.println();
        int[] array10second = new int[50];
        for (int i = 0; i < array10second.length; i++){
            if (array10[i] % 2 == 0){
                array10second[i] = array10[i];
            }
        }
        for(int i = 0 ; i < array10second.length; i++){
            System.out.print(array10second[i] + " ");
        }
        //11.   Создайте массив a, заполните его.
        // Создайте массив b, заполните его.
        // Создайте массив c, размер которого будет равен размер массива a + размер массива b.
        // Заполнить массив c следующим образом: взять элемент с массива a, следом элемент из массива b и т.д.
        System.out.println("Task11");
        int[] a = new int[5];
        int[] b = new int[6];
        int[] c = new int[a.length + b.length];
        for (int i = 0; i < a.length; i++) {
            a[i] = rand.nextInt(5);
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = rand.nextInt(5);
        }
        int check1 = 0;
        int check2 = 0;
        for (int i = 0; i < c.length; i++){
            if (i % 2 == 0){
                c[i] = b[check2];
                check2++;
            }else{
                c[i] = a[check1];
                check1++;
            }
        }
        for(int i = 0; i < c.length; i++){
            System.out.print(c[i] + " ");
        }
    }
}
